# myproject

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Run your end-to-end tests
```
npm run test:e2e
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


* Git global setup
* git config --global user.name "Rawich Loungsueb"
* git config --global user.email "60160130@go.buu.ac.th"
* 
* Create a new repository
* git clone https://gitlab.com/gametuapuanz/crud.git
* cd crud
* touch README.md
* git add README.md
* git commit -m "add README"
* git push -u origin master
* 
* Push an existing folder
* cd existing_folder
* git init
* git remote add origin https://gitlab.com/gametuapuanz/crud.git
* git add .
* git commit -m "Initial commit"
* git push -u origin master
* 
* Push an existing Git repository
* cd existing_repo
* git remote rename origin old-origin
* git remote add origin https://gitlab.com/gametuapuanz/crud.git
* git push -u origin --all
* git push -u origin --tags
